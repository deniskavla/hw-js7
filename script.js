// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//объектная модель документа, которая представляет все содержимое страницы в виде объектов, которые можно менять.

let x = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let ul = document.createElement('ul');
call_me(x);
document.body.append(ul);

let p = document.createElement('p');
p.id = "timer";

document.body.append(p);

// function call_me(params) {
//     for (let i = 0; i < params.length; i++) {
//         let li = document.createElement('li');
//         li.innerHTML = params[i];
//         ul.appendChild(li);
//     }
// }   это как способ сначала у меня так получилось, а потом уже с помощью шаблоных строк
function call_me(params) {
    let str = params.map(function (el) {
        return `<li>${el}</li>`;
    }).join('');
    ul.innerHTML = str;
}

let count = 10;
let counter = setInterval(timer, 1000);

function timer() {
    count = count - 1;
    if (count <= 0) {
        clearInterval(counter);
        ul.remove();
    }
    document.getElementById("timer").innerText = `${count} second`; // watch for spelling
}
